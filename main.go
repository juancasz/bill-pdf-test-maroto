package main

import (
	"fmt"

	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"github.com/johnfercher/maroto/pkg/props"
)

func main() {
	fmt.Println("Creating PDF...")

	m := pdf.NewMaroto(consts.Portrait, consts.A4)

	m.Row(250, func() {
		m.Col(12, func() {
			m.FileImage("base.jpg", props.Rect{
				Center:  true,
				Percent: 100,
			})
			m.Barcode("(415)7701234000011(8020)9876(3900)25500(96)20210605", props.Barcode{
				Top:     168,
				Left:    50,
				Percent: 50,
				Center:  false,
			})
		})
	})

	m.OutputFileAndClose("bill.pdf")

}
